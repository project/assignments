<?php

namespace Drupal\assignments\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Assignment type entities.
 */
interface AssignmentTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
